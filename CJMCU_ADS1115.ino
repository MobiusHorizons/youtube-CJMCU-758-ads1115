#include <Wire.h>
#include <U8g2lib.h>
#include <Adafruit_ADS1015.h>

U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0);
Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
double multiplier, currentMultiplier = 40;
int16_t offset = 0;

void print_digits(double value, short unsigned digits){
  double av = abs(value);
  while(av > 10){
    digits--;
    av/=10;
  }
  if (value >= 0){
    u8g2.print('+');
  }
  u8g2.print(value, digits);
}

void setup(void) 
{
  Serial.begin(9600);
  
  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
   ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV

  pinMode(0, INPUT_PULLUP);
  ads.begin();
  u8g2.begin();
  multiplier = 0.1875;
}

void loop(void) 
{
  int i;
  int32_t adc0 = 0, adc1 = 0;
  double current;

  for (i = 0; i < 10; i++){
    adc0 += ads.readADC_SingleEnded(0);
    adc1 += ads.readADC_SingleEnded(1);
  }
  adc0 /= 10;
  adc1 /= 10;

  if (digitalRead(0) == LOW){
    offset = (adc1 - (adc0/2));
  }

  current = (adc1 - (adc0/2) - offset) * multiplier / currentMultiplier;
  
  Serial.print("VIN : "); Serial.println(adc0 * multiplier / 1000, 6);
  Serial.print("VOUT: "); Serial.println(adc1 * multiplier / 1000, 6);
  Serial.print("AMPS: "); Serial.println(current, 6);

  Serial.println(" ");
  u8g2.firstPage();
  do {
    u8g2.setFont(u8g2_font_pressstart2p_8r);

    
    u8g2.setCursor(0, 8);
    u8g2.print("VIN :");
    print_digits(adc0 * multiplier / 1000, 6);
    u8g2.print("V");

    u8g2.setCursor(0, 24);
    u8g2.print("VOUT:");
    print_digits(adc1 * multiplier / 1000, 6);
    u8g2.print("V");

    u8g2.setCursor(0, 40);
    u8g2.print("AMPS:");
    print_digits(current, 6);
    u8g2.print("A");
  } while ( u8g2.nextPage() );
}
